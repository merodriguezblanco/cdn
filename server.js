var express      = require('express'),
    fs           = require('fs');

var serverPort = process.env.PORT || process.argv.pop();
serverPort = isNaN(serverPort) ? 9000 : serverPort;

var app = express();
var allowCrossDomain = function (request, response, next) {
  response.header('Access-Control-Allow-Origin', '*');
  response.header('Access-Control-Allow-Methods', 'GET');
  response.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

  next();
};

app.configure(function () {
  app.use(allowCrossDomain);
});

app.listen(serverPort);
console.log('Streaming Server listening on port ' + serverPort);

app.get('/', function (request, response) {
  var params = request.query,
      responseBody, chunkNumber, chunkStart, chunkSize, fileName, buffer;

  if (params !== {}) {
    chunkNumber = parseInt(params.chunk.number, 10);
    chunkStart  = parseInt(params.chunk.offset, 10);
    chunkSize   = parseInt(params.chunk.size, 10);
    fileName    = params.videoFileName.replace(/^.*[\\\/]/, '');

    fs.open(fileName, 'r', function (error, fileDescriptor) {
      if (error) {
        console.log('error ' + error.message);
        return;
      }

      buffer = new Buffer(chunkSize);
      fs.read(fileDescriptor, buffer, 0, chunkSize, chunkStart, function (error, num) {
        responseBody = { number: chunkNumber, data: buffer };
        response.json(200, responseBody || {});
      })
    });
  } else {
    response.json(400, {});
  }
});
